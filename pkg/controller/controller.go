package controller

import "demo-makefile/pkg/pb"

type controller struct {
	pb.UnsafeStudentServer
}

func NewController() *controller {
	return &controller{}
}
