package main

import (
	"demo-makefile/pkg/controller"
	"demo-makefile/pkg/pb"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	grpcServer := grpc.NewServer()
	server := controller.NewController()
	pb.RegisterStudentServer(grpcServer, server)

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("failed to start")
	}

	if err := grpcServer.Serve(listener); err != nil {
		log.Fatal("failed to serve")
	}
}
